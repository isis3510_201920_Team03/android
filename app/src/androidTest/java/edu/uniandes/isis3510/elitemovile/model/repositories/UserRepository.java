package edu.uniandes.isis3510.elitemovile.model.repositories;


        import java.util.List;

        import retrofit2.Call;
        import retrofit2.Callback;
        import retrofit2.Response;
        import retrofit2.Retrofit;
        import retrofit2.converter.gson.GsonConverterFactory;

        import edu.uniandes.isis3510.elitemovile.model.UserModel;

public class UserRepository {

    private static UserRepository userRepository;
    private List<UserModel> userList;

    private UserRepository() {
        //Establece comunicación DB

    }

    public synchronized static UserRepository getInstance() {
        //TODO No need to implement this singleton in Part #2 since Dagger will handle it ...
        if (userRepository == null) {
            if (userRepository == null) {
                userRepository = new UserRepository();
            }
        }
        return userRepository;
    }

    /*public LiveData<List<UserModel>> getUsersList() {
        final MutableLiveData<List<UserModel>> data = new MutableLiveData<>();

        gitHubService.getProjectList(userId).enqueue(new Callback<List<UserModel>>() {
            @Override
            public void onResponse(Call<List<Project>> call, Response<List<Project>> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<Project>> call, Throwable t) {
                // TODO better error handling in part #2 ...
                data.setValue(null);
            }
        });

        return data;
    }

    public LiveData<Project> getProjectDetails(String userID, String projectName) {
        final MutableLiveData<Project> data = new MutableLiveData<>();

        gitHubService.getProjectDetails(userID, projectName).enqueue(new Callback<Project>() {
            @Override
            public void onResponse(Call<Project> call, Response<Project> response) {
                simulateDelay();
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<Project> call, Throwable t) {
                // TODO better error handling in part #2 ...
                data.setValue(null);
            }
        });

        return data;
    }*/

    private void simulateDelay() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}