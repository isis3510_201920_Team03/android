package edu.uniandes.isis3510.elitemovile.model;

public class CompanyModel {
    private  final  int id;
    private final String name;
    private final String email;
    private final int phone;

    public static class Builder {
        //Params
        private final int id;
        private final String name;
        private final String email;
        private final int phone;
        public Builder(int pid, String pname, String pemail, int phone){
            this.id = pid;
            this.name = pname;
            this.email = pemail;
            this.phone = phone;
        }
        public CompanyModel build(){
            return new CompanyModel(this);
        }
    }

    public CompanyModel(Builder builder){
        id = builder.id;
        name = builder.name;
        email = builder.email;
        phone = builder.phone;
    }
    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public int getPhone() {
        return phone;
    }
}
