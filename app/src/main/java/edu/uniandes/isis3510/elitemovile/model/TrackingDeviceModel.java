package edu.uniandes.isis3510.elitemovile.model;

public class TrackingDeviceModel {
    private final int id;
    private final String serialNumb;
    private final boolean state;

    public static class Builder {
        //Params
        private final int id;
        private final String serialNumb;
        //Optional params
        private boolean state = false;

        public Builder(int pid, String pserialnumb) {
            this.id = pid;
            this.serialNumb = pserialnumb;
        }

        public Builder state(boolean pstate) {
            state = pstate;
            return this;
        }

        public TrackingDeviceModel build() {
            return new TrackingDeviceModel(this);
        }
    }

    private TrackingDeviceModel(Builder builder) {
        this.id = builder.id;
        this.serialNumb = builder.serialNumb;
        this.state = builder.state;
    }

    public int getId() {
        return id;
    }

    public String getSerialNumb() {
        return serialNumb;
    }

    public boolean isState() {
        return state;
    }
}
