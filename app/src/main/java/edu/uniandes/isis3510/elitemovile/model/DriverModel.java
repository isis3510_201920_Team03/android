package edu.uniandes.isis3510.elitemovile.model;


public class DriverModel {
    private  final  int id;
    private final String name;
    private final String firstname;
    private final int identifiation;
    private final int cellphone;

    public static class Builder {
        //Params
        private final int id;
        private final String name;
        private final String firstname;
        private final int identifiation;
        private final int cellphone;

        public Builder(int pid, String pname, String firstname, int identifiation, int cellphone){
            this.id = pid;
            this.name = pname;
            this.firstname = firstname;
            this.identifiation = identifiation;
            this.cellphone = cellphone;
        }
        public DriverModel build(){
            return new DriverModel(this);
        }
    }

    private DriverModel(Builder builder){
        id = builder.id;
        name = builder.name;
        firstname = builder.firstname;
        identifiation = builder.identifiation;
        cellphone = builder.cellphone;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public int getIdentifiation() {
        return identifiation;
    }

    public int getCellphone() {
        return cellphone;
    }
}
