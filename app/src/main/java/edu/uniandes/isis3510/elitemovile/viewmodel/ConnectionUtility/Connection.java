package edu.uniandes.isis3510.elitemovile.viewmodel.ConnectionUtility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class Connection {

    private static boolean estadoConexión(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if ((info == null || !info.isConnected() || !info.isAvailable())){
                Toast.makeText(context, "NO HAY CONEXIÓN A INTERNET ", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }


}
