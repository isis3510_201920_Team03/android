package edu.uniandes.isis3510.elitemovile.model;

import java.util.Date;

public class GPSLocationModel {
    private final int id;
    private final Date time;
    private final long lat;
    private final long lon;

    public static class Builder {
        //Params
        private final int id;
        private final Date time;
        private final long lat;
        private final long lon;
        public Builder(int pid, Date time, long plat, long plon){
            this.id = pid;
            this.time = time;
            this.lat = plat;
            this.lon = plon;
        }
        public GPSLocationModel build(){
            return new GPSLocationModel(this);
        }
    }

    private GPSLocationModel(Builder builder){
        time = builder.time;
        id = builder.id;
        lon = builder.lon;
        lat = builder.lat;
    }

    public int getId() {
        return id;
    }

    public Date getTime() {
        return time;
    }

    public long getLat() {
        return lat;
    }

    public long getLon() {
        return lon;
    }
}