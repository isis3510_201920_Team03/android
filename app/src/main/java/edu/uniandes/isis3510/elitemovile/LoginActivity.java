package edu.uniandes.isis3510.elitemovile;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import edu.uniandes.isis3510.elitemovile.viewmodel.fireStoreUtility.FireStoreCallBack;

import edu.uniandes.isis3510.elitemovile.model.UserModel;

public class LoginActivity extends AppCompatActivity {

    private final  static  String TAG_DEBUGG = "DEBUGG";

    private ArrayAdapter<UserModel> arrayAdapterUsers;
    private List<UserModel> listUsers = new ArrayList<UserModel>();

    private EditText nomUs, passUs, ageUs, emailUs;
    private ListView listview_users;

    private boolean existsUs = false;

    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    private final FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(this, getResources().getString(R.string.oncreate), Toast.LENGTH_SHORT).show();

        nomUs = findViewById(R.id.txt_name);
        passUs = findViewById(R.id.txt_password);
        ageUs = findViewById(R.id.txt_age);
        emailUs = findViewById(R.id.txt_email);

        listview_users = findViewById(R.id.lv_datosUsers);
        inicializarFirebase();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, getResources().getString(R.string.onstart), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, getResources().getString(R.string.onresume), Toast.LENGTH_SHORT).show();
    }

    //Actividad Corriendo


    @Override
    protected void onRestart() {
        super.onRestart();
        Toast.makeText(this, getResources().getString(R.string.onrestart), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, getResources().getString(R.string.onpause), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, getResources().getString(R.string.ondestroy), Toast.LENGTH_SHORT).show();
    }

    public void listarDatos(){
        databaseReference.child("User").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listUsers.clear();
                for(DataSnapshot objSnapshot : dataSnapshot.getChildren()){
                    UserModel us = objSnapshot.getValue(UserModel.class);
                    listUsers.add(us);

                    arrayAdapterUsers = new ArrayAdapter<UserModel>(LoginActivity.this, android.R.layout.simple_list_item_1, listUsers);
                    listview_users.setAdapter(arrayAdapterUsers);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void readData(final FireStoreCallBack callBack, final String email) {
        CollectionReference docRef = firebaseFirestore.collection("user/");
        docRef.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    boolean exis = false;
                    for (DocumentSnapshot document : task.getResult()) {
                        Object dat = document.getData();
                        Log.w(TAG_DEBUGG, "Data"+dat.toString());
                    }
                    callBack.onCallback(exis);
                } else {
                    Log.d(TAG_DEBUGG, "No such document");
                }
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String nom = nomUs.getText().toString();
        String age = ageUs.getText().toString();
        String pass = passUs.getText().toString();
        String email = emailUs.getText().toString();

        Log.w(TAG_DEBUGG, "");
        switch (item.getItemId()){
            case R.id.icon_add:{
                readData(new FireStoreCallBack() {
                             @Override
                             public void onCallback(boolean exist) {
                                 Log.w(TAG_DEBUGG, String.valueOf(exist));
                                 existsUs=exist;
                             }
                         }
                        , email);
                Log.w(TAG_DEBUGG, "Antes del if:"+String.valueOf(existsUs));
                if(nom.equals("")||pass.equals("")||age.equals("")||email.equals("")||existsUs){
                    validation();
                }else{
                    Toast.makeText( this, "Agregar", Toast.LENGTH_LONG).show();
                    UserModel user1 = new UserModel.Builder( 1, nom, pass, email, Integer.parseInt(age)).cityres("").build();
                    Map<String, UserModel> user2 = new HashMap<>();
                    user2.put("user", user1);
                    Toast.makeText(getApplicationContext(), "Create Registry", Toast.LENGTH_SHORT).show();
                    DocumentReference doc = firebaseFirestore.document("user/" + email);
                    doc.set(user2);
                    cleanBoxes();
                }

                Log.w(TAG_DEBUGG, "Despues del if "+String.valueOf(existsUs));
                break;
            }
            case R.id.icon_save:{
                Toast.makeText( this, "Guardar", Toast.LENGTH_LONG).show();
                break;
            }
            case R.id.icon_delete:{
                Toast.makeText( this, "Borrar", Toast.LENGTH_LONG).show();
                break;
            }
            default:break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void inicializarFirebase(){
        FirebaseApp.initializeApp(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }

    private void cleanBoxes(){
        nomUs.setText("");
        passUs.setText("");
        emailUs.setText("");
        ageUs.setText("");
    }

    private void validation(){
        String nom = nomUs.getText().toString();
        String age = ageUs.getText().toString();
        String pass = passUs.getText().toString();
        String email = emailUs.getText().toString();

        if(nom.equals("")){
            nomUs.setError("Name required");
        }if(age.equals("")){
            ageUs.setError("Age required");
        }if(pass.equals("")){
            passUs.setError("Password required");
        }if(email.equals("")){
            emailUs.setError("Email required");
        }if(existsUs){
            emailUs.setError("Usuario ya existe");
            Toast.makeText( this, "Ya existe un usuario con ese email", Toast.LENGTH_LONG).show();
        }
    }
}
