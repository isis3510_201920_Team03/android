package edu.uniandes.isis3510.elitemovile.viewmodel.fireStoreUtility;

import edu.uniandes.isis3510.elitemovile.model.UserModel;

public interface FireStoreCallBack {

    void onCallback(boolean existsUser);
}
