package edu.uniandes.isis3510.elitemovile.model;

public class UserModel {
    private int id;
    private String name;
    private int age;
    private String cityRes;
    private String email;
    private String password;

    public UserModel() {}
    public static class Builder {
        //Params
        private final int id;
        private final String name;
        private final String password;
        private final String email;
        private final int age;
        private String cityRes;
        public Builder(int pid, String pname, String ppassword, String pemail, int age){
            this.id = pid;
            this.name = pname;
            this.email = pemail;
            this.password = ppassword;
            this.age = age;
        }
        public Builder cityres(String pcityres)
            { cityRes = pcityres;   return this; }
        public UserModel build(){
            return new UserModel(this);
        }
    }

    private UserModel(Builder builder){
        id = builder.id;
        name = builder.name;
        age = builder.age;
        cityRes = builder.cityRes;
        email = builder.email;
        password = builder.password;
    }
    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getCityRes() {
        return cityRes;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
