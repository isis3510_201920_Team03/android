package edu.uniandes.isis3510.elitemovile.model;

public class BusModel {
    private  final  int id;
    private final String model;
    private final String licensePlate;

    public static class Builder {
        //Params
        private final int id;
        private final String model;
        private final String licensePlate;
        public Builder(int pid, String model, String licensePlate){
            this.id = pid;
            this.licensePlate = licensePlate;
            this.model = model;
        }
        public BusModel build(){
            return new BusModel(this);
        }
    }

    public BusModel(Builder builder){
        id = builder.id;
        model = builder.model;
        licensePlate = builder.licensePlate;
    }
    public int getId() {
        return id;
    }

    public String getModel() {
        return model;
    }

    public String getLicensePlate() {
        return licensePlate;
    }
}

