package edu.uniandes.isis3510.elitemovile.model;

public class RouteModel {
    private final int id;
    private final String description;

    public static class Builder {
        //Params
        private final int id;
        private final String description;
        public Builder(int pid, String pdesription){
            this.id = pid;
            this.description = pdesription;
        }
        public RouteModel build(){
            return new RouteModel(this);
        }
    }

    private RouteModel(Builder builder){
        id = builder.id;
        description = builder.description;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }
}
