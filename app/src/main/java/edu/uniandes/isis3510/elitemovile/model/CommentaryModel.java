package edu.uniandes.isis3510.elitemovile.model;

import java.util.Date;

public class CommentaryModel {
    private  final  int id;
    private final String description;
    private Date time;

    public static class Builder {
        //Params
        private final int id;
        private final String description;
        private Date time;
        public Builder(int pid, String description, Date time){
            this.id = pid;
            this.description = description;
            this.time = time;
        }
        public CommentaryModel build(){
            return new CommentaryModel(this);
        }
    }

    public CommentaryModel(Builder builder){
        id = builder.id;
        description = builder.description;
        time = builder.time;
    }
    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public Date getTime() {
        return time;
    }
}
