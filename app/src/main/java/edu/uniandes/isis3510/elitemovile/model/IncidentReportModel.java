package edu.uniandes.isis3510.elitemovile.model;

public class IncidentReportModel {
    private final int id;
    private final String type;
    private final String description;

    public static class Builder {
        //Params
        private final int id;
        private final String type;
        private final String description;
        public Builder(int pid, String type, String pdesription){
            this.id = pid;
            this.type = type;
            this.description = pdesription;
        }
        public IncidentReportModel build(){
            return new IncidentReportModel(this);
        }
    }

    private IncidentReportModel(Builder builder){
        id = builder.id;
        type = builder.type;
        description = builder.description;
    }

    public int getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }
}
