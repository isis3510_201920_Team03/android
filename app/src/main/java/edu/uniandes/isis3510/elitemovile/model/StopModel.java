package edu.uniandes.isis3510.elitemovile.model;


public class StopModel {
    private final int id;
    private final String name;
    private final long lat;
    private final long lon;

    public static class Builder {
        //Params
        private final int id;
        private final String name;
        private final long lat;
        private final long lon;
        public Builder(int pid, String pname, long plat, long plon){
            this.id = pid;
            this.name = pname;
            this.lat = plat;
            this.lon = plon;
        }
        public StopModel build(){
            return new StopModel(this);
        }
    }

    private StopModel(Builder builder){
        name = builder.name;
        id = builder.id;
        lon = builder.lon;
        lat = builder.lat;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getLat() {
        return lat;
    }

    public long getLon() {
        return lon;
    }
}
