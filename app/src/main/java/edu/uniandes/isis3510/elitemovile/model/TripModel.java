package edu.uniandes.isis3510.elitemovile.model;


import java.util.Date;

public class TripModel {
    private final int id;
    private final String origin;
    private final String destination;
    private final int cost;
    private final Date departure;
    private final Date arrival;
    private final Date aproxArriv;

    public static class Builder {
        //Params
        private final int id;
        private final String origin;
        private final String destination;
        private final int cost;
        private final Date departure;
        private final Date arrival;
        //Optional params
        private Date aproxArriv = null;

        public Builder(int pid, String porigin, String pdestination, int pcost, Date pdeparture, Date parrival){
            this.id = pid;
            this.origin = porigin;
            this.destination = pdestination;
            this.cost = pcost;
            this.departure = pdeparture;
            this.arrival = parrival;
        }
        public Builder aproxArriv(Date paproxarriv)
        { aproxArriv = paproxarriv;   return this; }
        public TripModel build(){
            return new TripModel(this);
        }
    }

    private TripModel(Builder builder){
        this.id = builder.id;
        this.origin = builder.origin;
        this.destination = builder.destination;
        this.cost = builder.cost;
        this.departure = builder.departure;
        this.arrival = builder.arrival;
        this.aproxArriv = builder.aproxArriv;
    }

    public int getId() {
        return id;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public int getCost() {
        return cost;
    }

    public Date getDeparture() {
        return departure;
    }

    public Date getArrival() {
        return arrival;
    }

    public Date getAproxArriv() {
        return aproxArriv;
    }
}

